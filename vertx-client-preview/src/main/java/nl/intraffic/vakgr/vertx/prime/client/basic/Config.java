package nl.intraffic.vakgr.vertx.prime.client.basic;

public class Config {

    public static final String TEAM_NAME = "ENTER_YOUR_TEAM_NAME";
    private static final boolean IS_TRAINING = false;
    public static final int WEB_PORT = 8080;

    public static final String HOST = "prime-arbiter.azurewebsites.net";
    public static final int PORT = 80;

    private static final String TRAINING = "/train/";
    public static final String BASE_ENDPOINT = IS_TRAINING ? TRAINING : "/";
    public static final String CANDIDATE = "candidate/";
    public static final String RESULT = "result/";

}
