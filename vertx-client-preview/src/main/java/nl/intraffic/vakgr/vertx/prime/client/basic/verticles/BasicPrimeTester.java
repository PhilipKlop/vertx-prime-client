package nl.intraffic.vakgr.vertx.prime.client.basic.verticles;

import com.google.gson.Gson;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import nl.intraffic.vakgr.vertx.prime.client.basic.model.internal.PrimeResult;
import nl.intraffic.vakgr.vertx.prime.client.basic.strategies.GottaCheckEmAll;

import java.math.BigInteger;

public class BasicPrimeTester extends AbstractVerticle {

    private final EventBus eventBus;
    private final Gson gson;

    public BasicPrimeTester(Vertx vertx) {
        eventBus = vertx.eventBus();
        gson = new Gson();
    }

    @Override
    public void start() {
        eventBus.consumer(Topic.CANDIDATE, this::handlePrimeCandidate);
    }

    private void handlePrimeCandidate(Message<String> message) {
        BigInteger candidate = gson.fromJson(message.body(), BigInteger.class);
        PrimeResult primeResult = new GottaCheckEmAll().execute(candidate);
        eventBus.publish(Topic.RESULT, gson.toJson(primeResult));
    }
}
