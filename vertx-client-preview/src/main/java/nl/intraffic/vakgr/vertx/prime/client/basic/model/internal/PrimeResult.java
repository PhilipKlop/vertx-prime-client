package nl.intraffic.vakgr.vertx.prime.client.basic.model.internal;

import java.math.BigInteger;

public class PrimeResult {

    private final BigInteger candidate;
    private final boolean isPrime;
    private final BigInteger divider;

    public static PrimeResult prime(BigInteger candidate) {
        return new PrimeResult(candidate, true, BigInteger.ONE);
    }

    public static PrimeResult composite(BigInteger candidate, BigInteger divider) {
        return new PrimeResult(candidate, false, divider);
    }

    private PrimeResult(BigInteger candidate, boolean isPrime, BigInteger divider) {
        this.candidate = candidate;
        this.isPrime = isPrime;
        this.divider = divider;
    }

    public BigInteger getCandidate() {
        return candidate;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public BigInteger getDivider() {
        return divider;
    }
}
