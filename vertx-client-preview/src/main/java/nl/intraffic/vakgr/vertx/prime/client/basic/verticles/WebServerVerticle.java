package nl.intraffic.vakgr.vertx.prime.client.basic.verticles;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import nl.intraffic.vakgr.vertx.prime.client.basic.Config;

public class WebServerVerticle extends AbstractVerticle {

    private final Vertx vertx;
    private final ThymeleafTemplateEngine engine;

    public WebServerVerticle(Vertx vertx) {
        this.vertx = vertx;
        engine = ThymeleafTemplateEngine.create();
    }

    @Override
    public void start() {
        Router router = createRouter();
        launchWebServer(router);
    }

    private Router createRouter() {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route(HttpMethod.GET, "/request").handler(this::handlePageRequest);
        router.route(HttpMethod.POST, "/request").handler(this::handleChallengeRequest);
        router.route("/*").handler(StaticHandler.create("static"));
        return router;
    }

    private void launchWebServer(Router router) {
        vertx.createHttpServer().requestHandler(router::accept).listen((Config.WEB_PORT), ar -> {
            if (ar.succeeded()) {
                System.out.println("WebServer is running, visit http://localhost:" + Config.WEB_PORT + "/request to get started.");
            } else {
                System.out.println("Verticle start failed: " + this.getClass().getSimpleName() + ar.cause());
            }
        });
    }

    private void handlePageRequest(RoutingContext context) {
        engine.render(context, "template/", "request.html", r -> handleRenderingResult(context, r));
    }

    private void handleChallengeRequest(RoutingContext context) {
        System.out.println("Requesting new challenge...");

        // TODO: 1. Add a topic.
        //  2. Publish to this topic using the eventbus.
        //  3. Let the ArbiterClient subscribe to it and process your request.

        if (true) throw new RuntimeException("Please do your homework");

        redirectPostToGet(context);
    }

    private void handleRenderingResult(RoutingContext context, AsyncResult<Buffer> response) {
        if (response.succeeded()) {
            context.response().end(response.result());
        } else {
            System.out.println("Rendering failed: " + response.cause());
            context.fail(response.cause());
        }
    }

    private static void redirectPostToGet(RoutingContext context) {
        context.response().putHeader("location", context.request().path()).setStatusCode(HttpResponseStatus.FOUND.code()).end();
    }
}
