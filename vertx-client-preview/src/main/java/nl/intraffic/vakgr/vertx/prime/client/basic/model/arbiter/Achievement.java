package nl.intraffic.vakgr.vertx.prime.client.basic.model.arbiter;

public class Achievement {

    private final String key;
    private final boolean ok;
    private final String duration;

    public Achievement(String key, boolean ok, String duration) {
        this.key = key;
        this.ok = ok;
        this.duration = duration;
    }

    public String getKey() {
        return key;
    }

    public boolean isOk() {
        return ok;
    }

    public String getDuration() {
        return duration;
    }
}
