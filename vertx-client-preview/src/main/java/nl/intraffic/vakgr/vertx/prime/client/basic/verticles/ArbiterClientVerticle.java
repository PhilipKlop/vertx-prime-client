package nl.intraffic.vakgr.vertx.prime.client.basic.verticles;

import com.google.gson.Gson;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import nl.intraffic.vakgr.vertx.prime.client.basic.model.arbiter.Achievement;
import nl.intraffic.vakgr.vertx.prime.client.basic.model.arbiter.Answer;
import nl.intraffic.vakgr.vertx.prime.client.basic.model.arbiter.Challenge;
import nl.intraffic.vakgr.vertx.prime.client.basic.model.internal.PrimeResult;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static nl.intraffic.vakgr.vertx.prime.client.basic.Config.*;

public class ArbiterClientVerticle extends AbstractVerticle {

    private final Gson gson;
    private final EventBus eventBus;
    private final WebClient arbiterClient;
    private final Map<BigInteger, String> challenges;

    public ArbiterClientVerticle(Vertx vertx) {
        this.eventBus = vertx.eventBus();
        gson = new Gson();
        arbiterClient = WebClient.create(vertx);
        challenges = new HashMap<>();
    }

    @Override
    public void start() {
        eventBus.consumer(Topic.RESULT, this::handleResult);
    }

    private void requestChallenge() {
        String endpoint = BASE_ENDPOINT + CANDIDATE + TEAM_NAME;
        arbiterClient.get(PORT, HOST, endpoint).send(this::handleChallenge);
    }

    private void handleChallenge(AsyncResult<HttpResponse<Buffer>> response) {
        if (response.succeeded()) {
            Challenge challenge = gson.fromJson(response.result().bodyAsString(), Challenge.class);
            System.out.println("Received challenge: " + challenge.getCandidate());
            BigInteger candidate = new BigInteger(challenge.getCandidate());
            challenges.put(candidate, challenge.getKey());
            eventBus.publish(Topic.CANDIDATE, gson.toJson(candidate));
        } else {
            System.out.println("Challenge request failed: " + response.cause());
        }
    }

    private void handleResult(Message<String> message) {
        PrimeResult result = gson.fromJson(message.body(), PrimeResult.class);
        String key = challenges.get(result.getCandidate());
        sendResult(key, result);
    }

    private void sendResult(String key, PrimeResult primeResult) {
        String response = gson.toJson(new Answer(key, primeResult.isPrime(), primeResult.getDivider().toString()));
        System.out.println(String.format("Sending prime result for %s: %s", primeResult.getCandidate(), response));
        arbiterClient.post(PORT, HOST, BASE_ENDPOINT + RESULT)
                .sendBuffer(Buffer.factory.buffer(response), this::handleAchievement);
    }

    private void handleAchievement(AsyncResult<HttpResponse<Buffer>> response) {
        if (response.succeeded()) {
            Achievement achievement = gson.fromJson(response.result().bodyAsString(), Achievement.class);
            System.out.println("Successful: " + achievement.isOk() + ", time: " +
                    achievement.getDuration().replace("PT", "") + " [key: " + achievement.getKey() + "]");
        } else {
            System.out.println("Result submission failed: " + response.cause());
        }
    }
}
