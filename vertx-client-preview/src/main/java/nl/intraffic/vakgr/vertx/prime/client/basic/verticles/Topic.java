package nl.intraffic.vakgr.vertx.prime.client.basic.verticles;

public class Topic {

    public static final String CANDIDATE = "prime.candidate";
    public static final String RESULT = "prime.result";

}
