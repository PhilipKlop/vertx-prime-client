package nl.intraffic.vakgr.vertx.prime.client.basic.model.arbiter;

public class Challenge {

    private final String key;
    private final String candidate;

    public Challenge(String key, String candidate) {
        this.key = key;
        this.candidate = candidate;
    }

    public String getKey() {
        return key;
    }

    public String getCandidate() {
        return candidate;
    }
}
