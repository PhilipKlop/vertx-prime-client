package nl.intraffic.vakgr.vertx.prime.client.basic.model.internal;

import java.math.BigInteger;

public class PositiveRange {

    private final BigInteger min;
    private final BigInteger max;

    public PositiveRange(BigInteger min, BigInteger max) {
        validateArguments(min, max);
        this.min = min.max(BigInteger.valueOf(2));
        this.max = max;
    }

    private void validateArguments(BigInteger min, BigInteger max) {
        if (min.compareTo(BigInteger.valueOf(0)) < 1) throw new IllegalArgumentException("Min not strictly positive");
        if (min.compareTo(max) > 0) throw new IllegalArgumentException("Max smaller than min");
    }

    public BigInteger getMin() {
        return min;
    }

    public BigInteger getMax() {
        return max;
    }
}
