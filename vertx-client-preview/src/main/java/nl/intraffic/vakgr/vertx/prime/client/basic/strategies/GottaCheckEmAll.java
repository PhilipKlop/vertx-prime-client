package nl.intraffic.vakgr.vertx.prime.client.basic.strategies;

import nl.intraffic.vakgr.vertx.prime.client.basic.model.internal.PrimeResult;

import java.math.BigInteger;

public class GottaCheckEmAll implements PrimeStrategy {

    @Override
    public PrimeResult execute(BigInteger candidate) {
        for (BigInteger i = TWO; i.compareTo(candidate) < 0; i = i.add(BigInteger.ONE)) {
            if (BigMath.isDivider(candidate, i)) {
                return PrimeResult.composite(candidate, i);
            }
        }
        return PrimeResult.prime(candidate);
    }
}
