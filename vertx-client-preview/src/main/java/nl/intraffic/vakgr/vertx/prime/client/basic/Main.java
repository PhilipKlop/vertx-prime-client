package nl.intraffic.vakgr.vertx.prime.client.basic;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import nl.intraffic.vakgr.vertx.prime.client.basic.verticles.BasicPrimeTester;
import nl.intraffic.vakgr.vertx.prime.client.basic.verticles.ArbiterClientVerticle;
import nl.intraffic.vakgr.vertx.prime.client.basic.verticles.WebServerVerticle;

public class Main {

    private static final DeploymentOptions DEPLOY_AS_WORKER = new DeploymentOptions().setWorker(true);

    public static void main(String[] args) {
        verifyTeamName();
        Vertx vertx = Vertx.vertx();
        deployWorkers(vertx);
        vertx.deployVerticle(new WebServerVerticle(vertx));
        vertx.deployVerticle(new ArbiterClientVerticle(vertx));
    }

    private static void verifyTeamName() {
        if (Config.TEAM_NAME.equals("ENTER_YOUR_TEAM_NAME")) throw new RuntimeException("Please enter your team name");
    }

    private static void deployWorkers(Vertx vertx) {
        vertx.deployVerticle(new BasicPrimeTester(vertx), DEPLOY_AS_WORKER);
    }
}
