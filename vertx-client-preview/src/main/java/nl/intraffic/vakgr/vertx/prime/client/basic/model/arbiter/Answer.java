package nl.intraffic.vakgr.vertx.prime.client.basic.model.arbiter;

public class Answer {

    private final String key;
    private final boolean isPrime;
    private final String divider;

    public Answer(String key, boolean isPrime, String divider) {
        this.key = key;
        this.isPrime = isPrime;
        this.divider = divider;
    }

    public String getKey() {
        return key;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public String getDivider() {
        return divider;
    }
}
