package nl.intraffic.vakgr.vertx.prime.client.basic.strategies;

import nl.intraffic.vakgr.vertx.prime.client.basic.model.internal.PrimeResult;

import java.math.BigInteger;

public interface PrimeStrategy {

    BigInteger TWO = BigInteger.valueOf(2L);
    BigInteger THREE = BigInteger.valueOf(3L);

    PrimeResult execute(BigInteger candidate);

    default PrimeResult executeOnRange(BigInteger candidate, BigInteger min, BigInteger max) {
        return execute(candidate);
    }
}
